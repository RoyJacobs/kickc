package dk.camelot64.kickc.model;

import dk.camelot64.kickc.model.statements.Statement;
import dk.camelot64.kickc.model.statements.StatementSource;
import org.antlr.v4.runtime.Token;

/** Signals some error in the code (or compilation) */
public class CompileError extends RuntimeException {

   private StatementSource source;

   public CompileError(String message) {
      super(message);
   }

   public CompileError(String message, StatementSource source) {
      super(getErrorPrefix(source) + message+"\n"+(source==null?"":source.toString()));
      this.source = source;
   }

   public CompileError(String message, Token token) {
      this(message, new StatementSource(token, token));
   }

   public CompileError(String message, Statement statement) {
      this(message, statement.getSource());
   }

   public CompileError(String message, Throwable cause) {
      super(message, cause);
   }

   public StatementSource getSource() {
      return source;
   }

   /**
    * Formats the StatementSource as a GCC-style error string prefix
    */
   private static String getErrorPrefix(StatementSource source) {
      if (source != null) {
         return source.getFileName() + ":" + source.getLineNumber() + ":" + source.getStartIndex() + ": error: ";
      } else {
         return "";
      }
   }
}
